package ictgradschool.industry.lab_concurrency.ex02;

import java.util.ArrayList;
import java.util.List;


public class ExerciseTwo {
    private int value = 0;

    private void start() throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            final int toAdd = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    add(toAdd);
                }
            });
            t.start();
            threads.add(t);
        }
        for (Thread t : threads) {
            t.join();
        }
        System.out.println("value = " + value);
    }

    private void add(int i) {
        value = value + i;
    }

    public static void main(String[] args) throws InterruptedException {
        new ExerciseTwo().start();
    }
}
/**
 * 1. What would you expect the output of this program to be?
 * //5050
 * 2. Is the output of the program guaranteed to be what you expect?
 * Why / why not? If not, what is the name of the problem which may occur,
 * and how could you change the program to mitigate this problem?
 */