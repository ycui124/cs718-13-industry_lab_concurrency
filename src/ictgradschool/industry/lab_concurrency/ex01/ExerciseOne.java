package ictgradschool.industry.lab_concurrency.ex01;

public class ExerciseOne {
    public static void main(String[] args) {
        /**
         * 1. Write code that declares an anonymous Runnable object with the variable name myRunnable.
         * The runnable should loop through all integers between 0 and 1 million, and print them out to the console.
         */
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    System.out.println(i);
                }
            }
        };
        /**
         *2. Write code that creates a Thread which will run myRunnable, then starts that thread.
         */
        Thread thread=new Thread(myRunnable);
        thread.start();
        /**
         * 3. Write code that will request that your thread gracefully terminates, then wait for it to finish.
         */
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
