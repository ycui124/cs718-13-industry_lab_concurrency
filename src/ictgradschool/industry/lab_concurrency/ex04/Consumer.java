package ictgradschool.industry.lab_concurrency.ex04;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BlockingQueue<Transaction> queue;
    private BankAccount a;

    public Consumer(BlockingQueue<Transaction> queue, BankAccount a) {
        this.queue = queue;
        this.a = a;
    }

    @Override
    public void run() {
        boolean n = true;
        try {
            while (n) {
                Transaction transaction = queue.take();
                switch (transaction._type) {
                    case Deposit:
                        a.deposit(transaction._amountInCents);
                        break;
                    case Withdraw:
                        a.withdraw(transaction._amountInCents);
                        break;
                }

            }
        } catch (InterruptedException e) {
          //  e.printStackTrace();
            n = false;
        }
    }
}
