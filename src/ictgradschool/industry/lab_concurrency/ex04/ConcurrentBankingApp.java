package ictgradschool.industry.lab_concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {
    public static void main(String[] args) {

        // Acquire Transactions to process.
        BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        List<Transaction> transactions = TransactionGenerator.readDataFile();
        BankAccount bankAccount = new BankAccount();

        System.out.println("Creating producer...");
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Transaction t : transactions) {
                    try {
                        queue.put(t);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "producer ");
        producer.start();


        System.out.println("Creating consumers...");
        List<Thread> consumers = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Consumer c = new Consumer(queue, bankAccount);
            Thread consumer = new Thread(c);
            consumer.start();
            consumers.add(consumer);
        }

        System.out.println("3 threads OK and ready to go.");
        System.out.println();
        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Thread t : consumers) {
            t.interrupt();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Final balance: " + bankAccount.getFormattedBalance());
    }

}
