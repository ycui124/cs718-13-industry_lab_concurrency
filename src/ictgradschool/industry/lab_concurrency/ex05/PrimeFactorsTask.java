package ictgradschool.industry.lab_concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {
    private long n;
    private List<Long> factors = new ArrayList<>();
    private TaskState state;

    public PrimeFactorsTask(long n) {
        this.n = n;
    }

    @Override
    public void run() {
        long n = this.n;
        System.out.println("starting "+n);
        // For each potential factor i.
        for (long i = 2; i * i <= n; i++) {
            System.out.println("RUNNING "+n);
            // If i is a factor of N, repeatedly divide it out.
            while (n % i == 0) {
                // if task state is ABORTED, then break

                factors.add(i);
                n = n / i;
                System.out.println("n======="+n);
            }
            if (state.equals(TaskState.ABORTED)) {
                return;
            }
        }

        // If biggest factor occurs only once, n > 1
        if (n > 1) {
            factors.add(n);
        }
        state = TaskState.COMPLETED;
    }

    public long n() {
        return n;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        return factors;
    }

    public TaskState getState() {

        return state;
    }

    public void setState(TaskState t) {
        this.state = t;
    }
}
