package ictgradschool.industry.lab_concurrency.ex05;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        App test = new App();
        try {
            test.test();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void test() throws InterruptedException {
        Scanner inputx = new Scanner(System.in);

        PrimeFactorsTask p = new PrimeFactorsTask(inputx.nextLong());
        Thread calculate = new Thread(p);
        p.setState(TaskState.INIGIZLIZED);
        calculate.start();

        Thread str = new Thread(new Runnable() {
            boolean isAbort = false;

            @Override
            public void run() {
                if (!(p.getState().equals(TaskState.COMPLETED))) {
                    Scanner input = new Scanner(System.in);
                    String enter = input.nextLine();
                    System.out.println("somebody enter something here");
                    p.setState(TaskState.ABORTED);
                }
            }
        });
        str.start();
        try {
            calculate.interrupt();
            str.interrupt();
            calculate.join();
            str.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
