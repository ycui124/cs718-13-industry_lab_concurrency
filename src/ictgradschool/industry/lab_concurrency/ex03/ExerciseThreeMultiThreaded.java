package ictgradschool.industry.lab_concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */


    @Override
    protected double estimatePI(long numSamples) {

        // TODO Implement this.
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        List<Thread> threadList = new ArrayList<>();
        double[] results = new double[5];
        for (int i = 0; i < 5; i++) {
            int currentNum = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    results[currentNum] = (ExerciseThreeMultiThreaded.super.estimatePI(numSamples));
                }
                 /*   long numInsideCircle = 0;
                    for (long j = 0; j < numSamples; j++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }

                    }
                    double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
                    estimatePIList.add(estimatedPi);
                }*/
            });
            thread.start();
            threadList.add(thread);
        }
        double sum = 0;
        for (int i = 0; i < 5; i++) {
            Thread t = threadList.get(i);
            try {

                t.join();
                sum += results[i];
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return sum/5;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
